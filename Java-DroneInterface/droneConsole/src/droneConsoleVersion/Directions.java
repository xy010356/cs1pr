package droneConsoleVersion;

public class Directions {
	public String directionX = "";
	public String directionY = "";
	
	public Directions(String x, String y) { // directions constructor
		directionX = x;
		directionY = y;
	}
	
	public String getDirectionX() { // getter
		return directionX;
	}
	
	public String getDirectionY() { // getter
		return directionY;
	}
	
	public void setDirectionX(String directionX) { // setter
		this.directionX = directionX;
	}
	
	public void setDirectionY(String directionY) { // setter
		this.directionY = directionY;
	}
	
	@Override
	public String toString() {
		return directionX + directionY; // return directions
	}
}
