package droneConsoleVersion;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;

public class DroneInterface {
	private Scanner s;								// scanner used for input from user
	   private ConsoleCanvas canvas;
	  
	    public DroneInterface() throws IOException {
	    	 s = new Scanner(System.in);			// set up scanner for user input
	    	 canvas = new ConsoleCanvas(10,10); // draw arena with size 10 by 10  	 
	    	 
	        char ch = ' ';
	        do {
	        	System.out.print("Press (D)isplay arena, Enter (A)dd drone, get (I)nformation, (M)ove drones once, Move drones (T)en times, (S)ave or e(X)it > ");
	        	ch = s.next().charAt(0);
	        	s.nextLine();
	        	switch (ch) {
	    			case 'A' :
	    			case 'a' :
	    			
	        			canvas.arena.addDrone();				// add new drones
	        			canvas.onMap();							// display new drones on the map
	       				break;
	       				
	        		case 'I' :
	        		case 'i' :
	        			
	        			System.out.print("\n" + arenaOutput());
	        			System.out.print(canvas.toString()); // displays the arena
	        			System.out.print(canvas.arena.toString()); // outputs the information for all drones
	           			break;
	           			
	        		case 'D' :
	        		case 'd' :
	        			
	        			System.out.print("\n" + arenaOutput());
	        			System.out.print(canvas.toString()); // displays the arena
	        			break;
	           			
	        		case 'M' :
	        		case 'm' :
	        			
	        			System.out.print("\n" + arenaOutput());
	        			moveAndDrawDrones(1); // call the function with parameter time = 1, hence drone/s will be moved once
	        			break;
	        			
	        		case 'T' :
	        		case 't' :
	        			
	        			System.out.print("\n" + arenaOutput());
	        			moveAndDrawDrones(10); // parameter time = 10, hence drone/s will be moved 10 times
	        			break;
	        			
	        		case 'S' :
	        		case 's' :
	        			
	        			writeToFile("textFile.txt"); 			// print information in text file
	        			break;
	        			
	        		case 'x' : 	
	        			
	        			ch = 'X';				// when X detected program ends
	        			break;
	        			
	        		}
	    		} while (ch != 'X');						// test if end
	        
	       s.close();									// close scanner
	    }
	    
	    public void moveAndDrawDrones(int times) { 
	    	
	    	for(int i=0; i<times; i++) {  // times is a parameter tells how many times the drone arena will be refreshed
	    	System.out.flush();
	    	try {
		    Thread.sleep(450); } // refresh the drone arena with the new positions of drones after 0.5sec
		    catch (Exception e) {
		    	System.out.println("error");
		    }
	    	canvas.arena.moveDrone(); // after each iteration - move the drone
			canvas.onMap(); // after each iteration - display the drones on the map
			System.out.print(canvas.toString()); // draw the arena
			System.out.print(canvas.arena.toString()); // present the new position of each drone
	    	}
	    }
	    
	    public void writeToFile(String fileName) 
	    	throws IOException {
	    	    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName)); // the parameter keeps the name of the file where the data will be stored
	    	    writer.write(arenaOutput()); // prints the size of the arena in the txt file
	    	    writer.newLine(); // makes a new line for the next content
	    	    for (var drone : canvas.arena.drones) { // goes through the array list of drones
					writer.write(drone.toString() + ", " + drone.movement); // prints the positions and directions in the txt file
					writer.newLine(); // display the result on a new line
				}
	    	    writer.close();
	    }
	    
	    public String arenaOutput() {
	    	return "The arena size is " + canvas.arena.sizeX + " " + "by" + " " + canvas.arena.sizeY + "\n";
	    }
	    
	    
		public static void main(String[] args) throws IOException {
			try {
				DroneInterface r = new DroneInterface(); // call the interface
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	// just call the interface
		}
}