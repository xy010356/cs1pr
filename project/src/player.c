 /*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static SDL_Texture *pete[4];
static SDL_Texture *reverse[4];
int frCnt = 0;
int aindex = 0;
int count;
int count2;
int a;
int b;
void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.entityTail->next = player;
	stage.entityTail = player;

	player->health = 1;

	pete[0] = loadTexture("gfx/edno1.png");
	pete[1] = loadTexture("gfx/edno2.png");
	pete[2] = loadTexture("gfx/edno3.png");
	pete[3] = loadTexture("gfx/edno4.png");

	reverse[0] = loadTexture("gfx/edno5.png");
	reverse[1] = loadTexture("gfx/edno6.png");
	reverse[2] = loadTexture("gfx/edno7.png");
	reverse[3] = loadTexture("gfx/edno8.png");

	b = sizeof(pete[0]);
	count = 24 / b;

	player->texture = pete[0];
	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void doPlayer(void)
{
	player->dx = 0;

	if(!app.keyboard[SDL_SCANCODE_A] && !app.keyboard[SDL_SCANCODE_D] && !app.keyboard[SDL_SCANCODE_S] && !app.keyboard[SDL_SCANCODE_I])
	{
		player->texture = pete[0];
	}

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;
		frCnt++;
		if((frCnt+1)%4 <= 0)
	{
			player->texture = reverse[aindex];
			aindex++;
		}
		else if(aindex >= count)
		{
				aindex = 0;
			}
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
				player->dx = PLAYER_MOVE_SPEED;
				frCnt++;
				if((frCnt+1)%4 <= 0)
			{
					player->texture = pete[aindex];
					aindex++;
				}
				else if(aindex >= count)
				{
						aindex = 0;
					}
			}

	if (app.keyboard[SDL_SCANCODE_I] &&  player->isOnGround)
	{
		player->riding = NULL;
		player->dy = -30;

    playSound(SND_JUMP, CH_PLAYER);
	}

	if (app.keyboard[SDL_SCANCODE_SPACE])
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_SPACE] = 0;
	}
}
