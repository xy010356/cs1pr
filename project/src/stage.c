/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);
static void drawMenu(void);
static void displaylevel(void);

//void dead(void);
int a;
void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap();

}

static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();


}

static void draw(void)
{
	SDL_SetRenderDrawColor(app.renderer, 128, 192, 255, 255);
	SDL_RenderFillRect(app.renderer, NULL);
	initCloud2();

	initCloud();

	initSun();

	drawMap();

	drawEntities();

	drawHud();

	drawMenu();

	displaylevel();
}

static void drawMenu(void)
{	if(a == 0)
	{
	SDL_Rect menu;

	menu.x = 0;
	menu.y = 0;
	menu.w = SCREEN_WIDTH;
	menu.h = 720;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 210);
	SDL_RenderFillRect(app.renderer, &menu);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
	drawText(620, 350, 255, 255, 255, TEXT_CENTER, "PRESS 'S' TO START", stage.pizzaFound, stage.pizzaTotal);
	drawText(620, 400, 255, 255, 255, TEXT_CENTER, "HOLD 'F' FOR INSTRUCTIONS", stage.pizzaFound, stage.pizzaTotal);
}

if(app.keyboard[SDL_SCANCODE_F])
{	a = 1;

	SDL_Rect inst;

	inst.x = 0;
	inst.y = 0;
	inst.w = SCREEN_WIDTH;
	inst.h = 720;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 210);
	SDL_RenderFillRect(app.renderer, &inst);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
	drawText(600, 300, 255, 255, 255, TEXT_CENTER, "'D'->RIGHT", stage.pizzaFound, stage.pizzaTotal);
	drawText(600, 350, 255, 255, 255, TEXT_CENTER, "'A'->LEFT", stage.pizzaFound, stage.pizzaTotal);
	drawText(600, 400, 255, 255, 255, TEXT_CENTER, "'I'->JUMP", stage.pizzaFound, stage.pizzaTotal);
}
if(app.keyboard[SDL_SCANCODE_S])
{
	a = 2;
}
}

// FULL SCREEN SDL
static void drawHud(void)
{
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "PIZZA %d/%d", stage.pizzaFound, stage.pizzaTotal);
}

static void displaylevel(void)
{	if(level.currentLevel == 0){

	SDL_Rect dis;

	dis.x = 0;
	dis.y = 0;
	dis.w = SCREEN_WIDTH;
	dis.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &dis);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(140, 5, 100, 100, 100, TEXT_RIGHT, "LEVEL 1", stage.pizzaFound, stage.pizzaTotal);
}
	else if(level.currentLevel == 1){
		SDL_Rect dis1;

		dis1.x = 0;
		dis1.y = 0;
		dis1.w = SCREEN_WIDTH;
		dis1.h = 35;

		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
		SDL_RenderFillRect(app.renderer, &dis1);
		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

		drawText(140, 5, 100, 100, 100, TEXT_RIGHT, "LEVEL 2", stage.pizzaFound, stage.pizzaTotal);

	}
	else if(level.currentLevel == 2){
		SDL_Rect dis2;

		dis2.x = 0;
		dis2.y = 0;
		dis2.w = SCREEN_WIDTH;
		dis2.h = 35;

		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
		SDL_RenderFillRect(app.renderer, &dis2);
		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

		drawText(210, 5, 100, 100, 100, TEXT_RIGHT, "FINAL LEVEL", stage.pizzaFound, stage.pizzaTotal);
	}
	else {
		SDL_Rect success;

		success.x = 0;
		success.y = 0;
		success.w = SCREEN_WIDTH;
		success.h = 720;

		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 255);
		SDL_RenderFillRect(app.renderer, &success);
		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

		drawText(620, 350, 255, 255, 255, TEXT_CENTER, "CONGRATULATIONS! YOU FINISHED THE GAME!", stage.pizzaFound, stage.pizzaTotal);
		drawText(620, 400, 255, 255, 255, TEXT_CENTER, "PRESS 'ESCAPE' TO EXIT THE GAME.", stage.pizzaFound, stage.pizzaTotal);
	}
	if(app.keyboard[SDL_SCANCODE_ESCAPE])
	{
		exit(1);
	}
}
/*
void dead(void)
{
	if(player->health == 0)
	{
	SDL_Rect dead;

	dead.x = 0;
	dead.y = 0;
	dead.w = SCREEN_WIDTH;
	dead.h = 720;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 210);
	SDL_RenderFillRect(app.renderer, &dead);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
	drawText(670, 350, 255, 255, 255, TEXT_CENTER, "YOU ARE DEAD!", stage.pizzaFound, stage.pizzaTotal);
}

}
*/
